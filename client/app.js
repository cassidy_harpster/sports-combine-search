import {Component, View} from 'angular2/core';

import {bootstrap} from 'angular2-meteor';

import {Players} from 'collections/players';

import {MeteorComponent} from 'angular2-meteor';

@Component({
  selector: 'app',
})
@View({
  templateUrl: 'client/main/main.html',
})

class PlayersComponent extends MeteorComponent {
  players: Mongo.Cursor<Object>;

  constructor() {
    super();
      this.subscribe('players', () => {
          this.players = Players.find();
      }, true);
  }

  search(value: string) {
    if (value) {
      this.players = Players.find({ lname: value });
    } else {
      this.players = Players.find();
    }
  }
}

bootstrap(PlayersComponent);
