import {Players} from 'collections/players';

export function loadPlayers() {
  if (Players.find().count() === 0){
    var players = [
      {
        "player": "CH-0000",
        "fname": "Cassidy",
        "lname": "Harpster",
        "pname": "C.Harpster",
        "pos1": "WR",
        "pos2": 0,
        "height": 67,
        "weight": 165,
        "yob": 1989,
        "forty": 5.5,
        "bench": 1,
        "vertical": 30.5,
        "broad": 10,
        "shuttle": 3.95,
        "cone": 6.79,
        "dpos": 54,
        "col": "UCF",
        "dv": "AAC",
        "start": 2015,
        "cteam": "JAX",
        "posd": "WR",
        "jnum": 1,
        "dcp": 1
      }
    ];

    for (var i = 0; i < players.length; i++) {
      Players.insert(players[i]);
    }
  }
}
