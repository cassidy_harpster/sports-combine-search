import {Players} from 'collections/players';

Meteor.publish('players', function() {
  return Players.find();
});
